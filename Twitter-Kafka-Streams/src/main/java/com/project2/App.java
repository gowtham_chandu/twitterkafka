package com.project2;

import org.apache.log4j.BasicConfigurator;

import com.project2.producer.TwitterKafkaProducer;

public class App {
    public static void main(String[] args) {
    	BasicConfigurator.configure();
        System.out.println("Stream Started-->");
        TwitterKafkaProducer producer = new TwitterKafkaProducer();
        producer.run();
    }
}