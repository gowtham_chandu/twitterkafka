# kafka_Streaming
This project is of implementing the kafka Streaming for feching live twitter tweets on real time.

For Running this user need Apache Kafka installed and the server running along with Zookeeper.
then create a kafka topic and name as hadoop.

As soon as the Application is started it will try to connect to the HBC Client which is for fetching the twitter data.
then the data will be shared to the Kafka Topic and then recieved by the Consumer.

For Getting the stream we need to open the Consumer Console with the same topic hadoop.

[Inf.java ](https://gitlab.com/virtusa-internship-program/kafka_streaming/-/blob/intern_dev/Twitter-Kafka-Streams/src/main/java/com/project2/config/Inf.java) This Class contains the information of the developer account keys and topic names.

[model ](https://gitlab.com/virtusa-internship-program/kafka_streaming/-/tree/intern_dev/Twitter-Kafka-Streams/src/main/java/com/project2/model)This Package contains the Structure of the output format we designed from the GSON format. Tweet contains Structure of tweet and User has USer data.

[Producer class ](https://gitlab.com/virtusa-internship-program/kafka_streaming/-/blob/intern_dev/Twitter-Kafka-Streams/src/main/java/com/project2/producer/TwitterKafkaProducer.java) The Controller and the main processing happens here.

For the more information about working and execution Go through this [Design Document](https://gitlab.com/virtusa-internship-program/kafka_streaming/-/blob/intern_dev/Twitter-Kakfa_Streaming_Report.pdf)

