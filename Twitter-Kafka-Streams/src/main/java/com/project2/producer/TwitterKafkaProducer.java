package com.project2.producer;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import com.google.gson.Gson;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import com.project2.config.Inf;
import com.project2.model.Tweet;

public class TwitterKafkaProducer {
    private Client client;
    private BlockingQueue<String> q;
    private Gson gson;

    public TwitterKafkaProducer() {
        Authentication auth = new OAuth1(Inf.CONSUMER_KEY,Inf.CONSUMER_SECRET,Inf.ACCESS_TOKEN,Inf.TOKEN_SECRET);
        StatusesFilterEndpoint ep = new StatusesFilterEndpoint();
        ep.trackTerms(Collections.singletonList(Inf.HASHTAG));
        q = new LinkedBlockingQueue<>(100);
        client = new ClientBuilder().hosts(Constants.STREAM_HOST).authentication(auth)
                .endpoint(ep).processor(new StringDelimitedProcessor(q)).build();
        gson = new Gson();
    }
    private Producer<Long, String> getProducer() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(properties);
    }

    public void run() {
        client.connect();
        try (Producer<Long, String> producer = getProducer()) {
        	int t=100;
            while (t>0) {
                Tweet tweet = gson.fromJson(q.take(), Tweet.class);
                System.out.printf("Fetched tweet id %d"+"\n", tweet.getId());
                long key = tweet.getId();
                String msg = tweet.toString();
                ProducerRecord<Long, String> record = new ProducerRecord<>(Inf.TOPIC, key, msg);
                producer.send(record);
                t--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            client.stop();
        }
    }
}
